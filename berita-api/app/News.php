<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    

    protected $fillable = ['title','image', 'content', 'category_id', 'user_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
   
    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if( empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()}= Str::uuid();
            }
            $model->user_id = auth()->user()->id;
        });
        
    }

    public function Category()
    {
        return $this->belongsTo('App\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function Comment()
    {
        return $this->hasMany('App\Comment');
    }

}   
