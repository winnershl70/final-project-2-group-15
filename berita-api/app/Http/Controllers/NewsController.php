<?php

namespace App\Http\Controllers;

use App\News;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::latest()->get();
        
        return response()->json([
            'success' => true,
            'message' => 'list data category',
            'data' => $news
        ], 200); 
    }


    public function random($item)
    {
        
      
        $news = News::inRandomOrder()
                     ->limit($item)
                     ->paginate(4);
    
            return response()->json([
                'success' => true,
                'message' => 'List Data news',
                'data'    => $news  
            ], 200);
        
       
        
    }


    public function kategori($category_id)
    {
        
        $news = News::latest()
                      ->where('category_id','=', $category_id)
                      ->paginate(4);
    
            return response()->json([
                'success' => true,
                'message' => 'List Data news',
                'data'    => $news  
            ], 200);
        
       
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
                'title' => 'required',
                'content' => 'required',
                'category_id' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg',

               
        ]);

     

        //validasi error
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        } 
          
       
        $namaGambar = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('image'), $namaGambar);
        
        //save ke database
        $news = News::create([
            'title' => $request->title,
            'content' => $request->content,
            'category_id' => $request->category_id,
            'image' => $namaGambar, 
                
        ]);
       
        
        if ($news) {
            return response()->json([
                'success' => true,
                'message' => 'berhasil save data news',
                'data' => $news
            ], 201);
        }

        //gagal save
        return response()->json([
            'success' => false,
            'message' => 'gagal save data'
        ], 409);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::findOrFail($id);

        //response json-nya
        return response()->json([
           'success' => true,
           'message' => 'data news by id',
           'data'    => $news
       ], 200);
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id)
    {
            $allRequest = $request->all();

            $validator = Validator::make($allRequest, [
                'title' => 'required',
                'content' => 'required',
                'image' => 'image|mimes:jpeg,png,jpg',
            ]);
    
            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            } 
            $news = News::find($id);
        if($request->has('image')){
            $user = auth()->user()->id;
            if($news->user_id != $user){

                return response()->json([
                    'success' => false,
                    'message' => 'data news ini bukan milik user '
                ], 403);
    
            }
                // update data
            $path = 'image/';
            File::delete($path . $news->image);

            $namaGambar = time().'.'.$request->image->extension();  
     
            $request->image->move(public_path('image'), $namaGambar);

                $news->update([
                    'title' => $request->title,
                    'image' => $namaGambar,
                    'content' => $request->content,
                    
                ]);
     
                return response()->json([
                    'success' => true,
                    'message' => 'news data Updated',
                    'data'    => $news  
                ], 200);
     
        }else{
            $user = auth()->user();
            if($news->user_id != $user->id){

                return response()->json([
                    'success' => false,
                    'message' => 'data news ini bukan milik user '
                ], 403);
    
            }
            $news->update([
                'title' => $request->title,
                'content' => $request->content,
            ]);
 
            return response()->json([
                'success' => true,
                'message' => 'news data Updated',
                'data'    => $news  
            ], 200);
        }
         //data post not found
         return response()->json([
            'success' => false,
            'message' => 'news Not Found',
        ], 404);
       
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);

        $news->delete();
        return response()->json([
            'success' => true,
            'message' => 'data news berhasil di hapus', 
        ], 200);
    }
}
