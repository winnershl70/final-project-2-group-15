<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'ini data comments',
            'data'    => $comments  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'content'   => 'required',
            'news_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comments = Comment::create([
            'content'  => $request->content,
            'news_id'  => $request->news_id
        ]);

        //memmanggil event CommentStoredEvent
        // event(new CommentStoredEvent($comments)); 

        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'berhasil tambah comment',
                'data'    => $comments  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'gagal tambah data',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comment::find($id);

        if($comments)
        {
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data comments',
                'data'    => $comments 
            ], 200);
        }
        return response()->json([
            'success' =>  false,
            'message' =>  'data dengan id : '. $id. 'tidak ditemukan',
        ], 404);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'content'  => 'required',
            'news_id' => 'required',
        ]);

        if($validator->fails() ){
            return response()->json($validator->errors(), 400);
        }

        $comments = Comment::find($id);

        if($comments)
        {
            $user = auth()->user();
            if($comments->user_id != $user->id){

                return response()->json([
                    'success' => false,
                    'message' => 'data comments ini bukan milik user '
                ], 403);
    
            }
            $comments->update([
                'content'  => $request->content,
                'news_id'  => $request->news_id
            ]);

            return response()->json([
                'success' =>  true,
                'message' =>  'data dengan judul : '.$comments->content.' berhasil di update',
                'data' =>     $comments
            ]);
        }

        return response()->json([
            'success' =>  false,
            'message' =>  'data dengan id : '.$id.' tidak ada',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = Comment::find($id);

        if($comments) {

            $user = auth()->user();
            if($comments->user_id != $user->id){

                return response()->json([
                    'success' => false,
                    'message' => 'data comments ini bukan milik user '
                ], 403);
    
            }
            //delete post
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'comment berhasil dihapus'
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'comment tidak di temukan',
        ], 404);
    }
}
