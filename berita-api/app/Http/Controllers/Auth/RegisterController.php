<?php

namespace App\Http\Controllers\Auth;

use App\Otp;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $user = User::create($allRequest);

        do {
            $random = mt_rand( 100000 , 999999);
            $check = Otp::where('otp', $random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_code = Otp::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);


        //kirim email ke penerima
        try{
            Mail::send('mails/register_email_user', array('pesan' => $otp_code->otp) , function($pesan) use($request){
                $pesan->to($request->email,'Verifikasi')->subject('Verifikasi Email');
                $pesan->from(env('MAIL_USERNAME','newsapp240@gmail.com'),'Verifikasi Akun email anda');
            });
        }catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
        return response()->json([
            'success' => true,
            'massage' => 'data user success created',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code,
            ]
            ]);
    }

}
