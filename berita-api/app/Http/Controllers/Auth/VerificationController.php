<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Otp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'otp' => 'required'

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();



        $otp_code = Otp::where('otp', $request->otp)->first();

        if(!$otp_code){
            return response()->json([
                'success'=> false,
                'massage'=> 'OTP Code Not Found',
            ], 400);
        }


        $now = Carbon::now();

        if( $now > $otp_code->valid_until)
        {
            return response()->json([
                'success'=> false,
                'massage'=> 'OTP Code Expired',
            ], 400);
        }

        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();

        return response()->json([
            'success'=> true,
            'massage'=> 'Verified User Success',
            'data' => $user
        ], 200);
    }
}
