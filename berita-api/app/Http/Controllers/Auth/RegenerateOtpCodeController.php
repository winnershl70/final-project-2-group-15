<?php

namespace App\Http\Controllers\Auth;

use Mail;
use App\Otp;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use App\Events\UserOtpStoredEvent;
use App\Http\Controllers\Controller;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegenerateOtpCodeController extends Controller

{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'email' => 'required'

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        // if ($user->otp_code) {
        //     $user->otp_code->delete();
        // }

        do {
            $random = mt_rand( 100000 , 999999);
            $check = Otp::where('otp', $random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_code = Otp::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);

        // kirim emall
<<<<<<< HEAD
         try{
            Mail::send('mails/register_email_user', array('pesan' => $otp_code->otp) , function($pesan) use($request){
                $pesan->to($request->email,'Verifikasi')->subject('Verifikasi Email');
                $pesan->from(env('MAIL_USERNAME','newsapp240@gmail.com'),'Verifikasi Akun email anda');
            });
        }catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
=======
        //  try{
        //     Mail::send('mails/register_email_user', array('pesan' => $otp_code->otp) , function($pesan) use($request){
        //         $pesan->to($request->email,'Verifikasi')->subject('Verifikasi Email');
        //         $pesan->from(env('MAIL_USERNAME','newsapp240@gmail.com'),'Verifikasi Akun email anda');
        //     });
        // }catch (Exception $e){
        //     return response (['status' => false,'errors' => $e->getMessage()]);
        // }

        event(new UserOtpStoredEvent($otp_code));
>>>>>>> 78adb7e0249c8ba8adfc78880c29566b339eee64

        return response()->json([
            'success' => true,
            'massage' => 'otp code succes to generate',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code,
            ]
            ]);
    }
}
