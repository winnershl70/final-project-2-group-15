<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'ini data role',
            'data'    => $role  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'name'   => 'required',
            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $role = Role::create([
            'name'  => $request->name,
            
        ]);

        //memmanggil event CommentStoredEvent
      
        if($role) {

            return response()->json([
                'success' => true,
                'message' => 'berhasil tambah role',
                'data'    => $role  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'gagal tambah data',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);

        if($role)
        {
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data comments',
                'data'    => $role 
            ], 200);
        }
        return response()->json([
            'success' =>  false,
            'message' =>  'data dengan id : '. $id. 'tidak ditemukan',
        ], 404);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'content'  => 'required',
            'post_id' => 'required',
        ]);

        if($validator->fails() ){
            return response()->json($validator->errors(), 400);
        }

        $role = Role::find($id);

        if($role)
        {
            $role->update([
                'name'  => $request->name,
                
            ]);

            return response()->json([
                'success' =>  true,
                'message' =>  'role di update',
                'data' =>     $role
            ]);
        }

        return response()->json([
            'success' =>  false,
            'message' =>  'data role tidak ada',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);

        if($role) {
            //delete post
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'role berhasil dihapus'
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'role tidak di temukan',
        ], 404);
    }
}
