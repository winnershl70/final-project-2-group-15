<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $category = Category::latest()->get();
        
        return response()->json([
            'success' => true,
            'message' => 'list data category',
            'data' => $category
        ], 200); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
                'name' => 'required',
               
        ]);

        //validasi error
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        } 

        //save ke database
        $category = Category::create([
            'name' => $request->name,
            
        ]);

        if ($category) {
            return response()->json([
                'success' => true,
                'message' => 'berhasil save data category',
                'data' => $category
            ], 201);
        }

        //gagal save
        return response()->json([
            'success' => false,
            'message' => 'gagal save data'
        ], 409);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);

        //response json-nya
        return response()->json([
           'success' => true,
           'message' => 'list data category',
           'data'    => $category
       ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  Category $category)
    {
        //ambil semua data
        $allRequest = $request->all();

        //validation data role
        $validator = Validator::make($allRequest, [
            'name' => 'required',
           
        ]);
        
       //validation error
       if($validator->fails()){
           return response()->json($validator->errors(), 400);
       } 

       $category = Category::findOrFail($category->id);
       
       if($category){
       // update data
           $category->update([
               'name' => $request->name,
               
           ]);

           return response()->json([
               'success' => true,
               'message' => 'category data Updated',
               'data'    => $category  
           ], 200);

        }

            //data post not found
            return response()->json([
                'success' => false,
                'message' => 'category Not Found',
            ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);

        $category->delete();
        return response()->json([
            'success' => true,
            'message' => 'data category berhasil di hapus', 
        ], 200);
    }
}
