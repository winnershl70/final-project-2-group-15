<?php

namespace App\Listeners;

use App\Events\UserOtpStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToRegenerateOtp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserOtpStoredEvent  $event
     * @return void
     */
    public function handle(UserOtpStoredEvent $event)
    {
        try{
            Mail::send('mails/register_email_user', array('pesan' => $otp_code->otp) , function($pesan) use($request){
                $pesan->to($request->email,'Verifikasi')->subject('Verifikasi Email');
                $pesan->from(env('MAIL_USERNAME','newsapp240@gmail.com'),'Verifikasi Akun email anda');
            });
        }catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
        }

    }
}
