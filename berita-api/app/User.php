<?php

namespace App;


use Illuminate\Notifications\Notifiable;

use App\Role;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use App\User;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $fillable = ['username', 'name', 'email', 'email_verified_at', 'password', 'role_id'];
    protected $primaryKey = 'id';
    protected $keyTape = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->role_id = Role::where('name', 'admin')->first()->id;
        });
    }
     
    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function news()
    {
        return $this->hasMany('App\News');
    }
    public function Comment()
    {
        return $this->hasMany('App\Comment');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function otp_code()
    {
        return $this->hasOne('App\Otp');
    }

}
