##group - 15
1.dzaki sholahuddin email : winnershl70@gmail.com
2.kharis luqman email : kharisluqman5@gmail.com
3.alya shafwa wafiya email : shafwa.alya@gmail.com

##link web service api :

https://group15-api.herokuapp.com

##link publish postman :

https://documenter.getpostman.com/view/18400931/UVR7KoA2#26503e36-ea4e-4daa-89f5-fc6b3c8587f3

## Tema Project

Dalam project ini, kami membuat **Sistem Informasi Berita (SIB)**. Sistem ini memiliki 3 fungsi utama, yaitu :
1. Mengelola data Berita
2. Mengelola data comment


## ERD 
<img src="erd-berita-v-1.1.png">

##email
<img src="email.png">

